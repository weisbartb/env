package env

import "github.com/armon/go-radix"

// External contracts that can be implemented

type SafeTree struct {
	tree *radix.Tree
}

func (st SafeTree) LongestPrefix(s string) (key string, value interface{}, found bool) {
	return st.tree.LongestPrefix(s)
}

// The type of the node value is TreeNode as a value
func (st SafeTree) WalkPrefix(prefix string, fn radix.WalkFn) {
	st.tree.WalkPrefix(prefix, fn)
}

// Unmarshal a custom data type
type Unmarshal interface {
	// Unmarshal string bytes into the existing struct
	// The tree pointer only exposes a few endpoints
	// Return an error if it can't unmarshal
	// A returned error will stop the encoding
	UnmarshalEnv(data []byte, tree SafeTree) error
}
