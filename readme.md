# Environment Variable Unmarshaller

This tool assists in managing configuration overrides from environment variables. This tool will canonicalize environment variable names as well as tag names with one exception. If dynamic map expansion is enabled it will use the original values for the map keys. 




## Requirements
* Go 1.11+ (earlier may be supported but hasn't been tested)
* Go Modules (vgo) Enabled

## Included packages
* github.com/armon/go-radix v1.0.0 - Used to provide dynamic expansion for maps
* github.com/valyala/fastjson v1.4.1 - Used to parse through potential JSON values

## Usage

#### Canonicalization
Given the struct
```
type example struct {
    SomeWeirdVariable string `env:"someWeirdVariable"`
}
```
```
myapp__SomeWeirdVaRiAble="test"
``` 
Is equivalent to 
```
myapp__SomeWeirdVariable="test"
``` 
Both will properly set the above structure to the value of `test`

#### Map expansion
This tool will automatically structure maps based on the given path.

Given the struct
```
type Route struct {
    Location string `env:"location"`
    Timeout int64 `env:"timeout"`
}
type exampleConfig struct {
    Routes map[string]Route `env:"routes"`
}
```
The following env variables
```
myapp__routes__authenticationService= {"location" : "https://some-url/authentication", "timeout" : 1000}
myapp__routes__formService= {"location" : "https://some-url/form", "timeout" : 1000}
myapp__routes__configService= {"location" : "https://some-url/config", "timeout" : 1000}
```
Would expand into
```
Routes : {
    "authenticationService" : {
        "location" : "https://some-url/authentication",
        "timeout" : 1000
    },
    "formService" : {
        "location" : "https://some-url/form",
        "timeout" : 1000
    },
    "configService" : {
        "location" : "https://some-url/config",
        "timeout" : 1000
    }   
}
```