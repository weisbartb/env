// This file contains a list of all of the decoders used for resolving various primitive values

package env

import (
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"reflect"
	"strconv"
	"strings"

	"github.com/armon/go-radix"
	"github.com/valyala/fastjson"
)

// Various value errors
var ErrTooLargeInt = errors.New("integer value is too large to fit in int")
var ErrTooLargeInt8 = errors.New("integer value is too large to fit in int8")
var ErrTooLargeInt16 = errors.New("integer value is too large to fit in int16")
var ErrTooLargeInt32 = errors.New("integer value is too large to fit in int32")
var ErrTooLargeInt64 = errors.New("integer value is too large to fit in int64")
var ErrTooLargeUint = errors.New("integer value is too large to fit in uint")
var ErrTooLargeUint8 = errors.New("integer value is too large to fit in uint8")
var ErrTooLargeUint16 = errors.New("integer value is too large to fit in uint16")
var ErrTooLargeUint32 = errors.New("integer value is too large to fit in uint32")
var ErrTooLargeUint64 = errors.New("integer value is too large to fit in uint64")
var ErrUnableToResolve = errors.New("unable to resolve value to a decoder")
var ErrTooLargeFloat32 = errors.New("float is too large to fit in float32")
var ErrJsonRequired = errors.New("JSON is required for struct unmarshalling")

// Holder for the resolver, in case it needs to be extended at a later date with extra data/arguments
type valueDecoder struct {
	Resolver ValueResolver
}

// Holds the value that will be applied to the  object, contains the JSON if any that was present
type valueHolder struct {
	// Stringed value
	stringValue string
	// The radix keyspace
	envSpace *radix.Tree
	// json decoded value if present
	jsonValue *fastjson.Value
	// Current prefix
	activePrefix string
	// Spacer
	envSpacer string
}

// Resolve an underlying type
func (t valueDecoder) resolve(dispatchTable valueResolutionDispatch, valueObj valueHolder, dynamicType reflect.Type) (target reflect.Value, err error) {
	return t.Resolver(dispatchTable, valueObj, dynamicType)
}

type valueResolutionDispatch map[reflect.Kind]valueDecoder

func (rm valueResolutionDispatch) resolve(value reflect.Kind) (valueDecoder, bool) {
	if tmp, ok := rm[value]; ok {
		return tmp, true
	} else {
		return valueDecoder{}, false
	}
}

type ValueResolver func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (reflect.Value, error)

// handles decoding of string and json values
// Attempts to use the JSON value first, then fall back to string value
var allowedValues = valueResolutionDispatch{
	reflect.Bool: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp bool
			// See if the item is embedded in a json object
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Bool(); err == nil {
					target.SetBool(tmp)
				}
				return
			}
			// Fallback to string parse
			if tmp, err = strconv.ParseBool(valueObj.stringValue); err == nil {
				target.SetBool(tmp)
			}
			return
		},
	},
	reflect.Int: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp int64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Int64(); err == nil {
					// Ensure that the platform specific "int" doesn't overflow on value
					if uint64(tmp) > uint64(1<<(uint(target.Type().Bits()-1))) {
						err = ErrTooLargeInt
						return
					}
					target.SetInt(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseInt(valueObj.stringValue, 10, target.Type().Bits()); err == nil {
				target.SetInt(tmp)
			}
			return
		},
	},
	reflect.Int8: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp int64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Int64(); err == nil {
					if tmp > maxInt8 {
						err = ErrTooLargeInt8
						return
					}
					target.SetInt(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseInt(valueObj.stringValue, 10, 8); err == nil {
				target.SetInt(tmp)
			}
			return
		},
	},
	reflect.Int16: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp int64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Int64(); err == nil {
					if tmp > maxInt16 {
						err = ErrTooLargeInt16
						return
					}
					target.SetInt(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseInt(valueObj.stringValue, 10, 16); err == nil {
				target.SetInt(tmp)
			}
			return
		},
	},
	reflect.Int32: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp int64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Int64(); err == nil {
					if tmp > maxInt32 {
						err = ErrTooLargeInt32
						return
					}
					target.SetInt(tmp)
				}
				return
			}
			if tmpInt, err := strconv.ParseInt(valueObj.stringValue, 10, 32); err == nil {
				target.SetInt(tmpInt)
			}
			return
		},
	},
	reflect.Int64: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp int64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Int64(); err == nil {
					if tmp > maxInt64 {
						err = ErrTooLargeInt64
						return
					}
					target.SetInt(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseInt(valueObj.stringValue, 10, 64); err == nil {
				target.SetInt(tmp)
			}
			return
		},
	},
	reflect.Uint: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp uint64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Uint64(); err == nil {
					if tmp > uint64(1<<(uint(target.Type().Bits()-1))) {
						err = ErrTooLargeUint
						return
					}
					target.SetUint(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseUint(valueObj.stringValue, 10, target.Type().Bits()); err == nil {
				target.SetUint(tmp)
			}
			return
		},
	},
	reflect.Uint8: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp uint64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Uint64(); err == nil {
					if tmp > maxUint8 {
						err = ErrTooLargeUint8
						return
					}
					target.SetUint(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseUint(valueObj.stringValue, 10, 8); err == nil {
				target.SetUint(tmp)
			}
			return
		},
	},
	reflect.Uint16: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp uint64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Uint64(); err == nil {
					if tmp > maxUint16 {
						err = ErrTooLargeUint16
						return
					}
					target.SetUint(tmp)
					return
				}
			}
			if tmp, err = strconv.ParseUint(valueObj.stringValue, 10, 16); err == nil {
				target.SetUint(tmp)
			}
			return
		},
	},
	reflect.Uint32: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp uint64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Uint64(); err == nil {
					if tmp > maxUint32 {
						err = ErrTooLargeUint32
						return
					}
					target.SetUint(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseUint(valueObj.stringValue, 10, 32); err == nil {
				target.SetUint(tmp)
			}
			return
		},
	},
	reflect.Uint64: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp uint64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Uint64(); err == nil {
					if tmp > maxUint64 {
						err = ErrTooLargeUint64
						return
					}
					target.SetUint(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseUint(valueObj.stringValue, 10, 64); err == nil {
				target.SetUint(tmp)
			}
			return
		},
	},
	reflect.Float32: {
		func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp float64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Float64(); err != nil {
					if tmp > math.MaxFloat32 {
						err = ErrTooLargeFloat32
					}
					target.SetFloat(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseFloat(valueObj.stringValue, 32); err == nil {
				target.SetFloat(tmp)
			}
			return
		},
	},
	reflect.Float64: {
		func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			var tmp float64
			if valueObj.jsonValue != nil {
				if tmp, err = valueObj.jsonValue.Float64(); err == nil {
					target.SetFloat(tmp)
				}
				return
			}
			if tmp, err = strconv.ParseFloat(valueObj.stringValue, 64); err == nil {
				target.SetFloat(tmp)
			}
			return
		},
	},
	reflect.String: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType).Elem()
			if valueObj.jsonValue != nil {
				target.SetString(string(valueObj.jsonValue.GetStringBytes()))
			} else {
				target.SetString(valueObj.stringValue)
			}
			return
		},
	},
	reflect.Slice: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			if items, err := valueObj.jsonValue.Array(); err == nil {
				target = reflect.MakeSlice(targetType, len(items), len(items))
				for k, v := range items {
					if decoder, ok := valueResolverMap.resolve(target.Type().Elem().Kind()); ok {
						if val, resolverError := decoder.resolve(valueResolverMap, valueHolder{jsonValue: v}, target.Type().Elem()); resolverError == nil {
							target.Index(k).Set(val)
						} else {
							err = resolverError
						}
					} else {
						err = ErrUnableToResolve
					}
				}
			} else {
				target = reflect.MakeSlice(targetType.Elem(), 0, 0)
			}
			return
		},
	},
	reflect.Struct: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.New(targetType)
			if len(valueObj.stringValue) > 0 && (valueObj.stringValue[0] == '{' || valueObj.stringValue[0] == '[') {
				err = json.Unmarshal([]byte(valueObj.stringValue), target.Interface())
			} else {
				err = ErrJsonRequired
			}
			return
		},
	},
	reflect.Map: {
		Resolver: func(valueResolverMap valueResolutionDispatch, valueObj valueHolder, targetType reflect.Type) (target reflect.Value, err error) {
			target = reflect.MakeMap(targetType)
			mapValueKind := target.Type().Elem().Kind()
			if valueTypeOf, valueResolverFound := valueResolverMap[mapValueKind]; valueResolverFound {
				if valueObj.jsonValue != nil {
					var obj *fastjson.Object
					if obj, err = valueObj.jsonValue.Object(); err == nil {
						var outerErr error
						func() {
							defer func() {
								if r := recover(); r != nil {
									outerErr = r.(error)
								}
							}()
							obj.Visit(func(jsonKey []byte, jsonVal *fastjson.Value) {
								if value, resolverError := valueTypeOf.resolve(valueResolverMap, valueHolder{jsonValue: jsonVal}, target.Type().Elem()); resolverError == nil {
									target.SetMapIndex(reflect.ValueOf(string(jsonKey)), value)
								} else {
									panic(resolverError)
								}
							})
						}()
						if outerErr != nil {
							err = outerErr
							return
						}
					}
					return
				}
				// Switch to fragment mode
				// See if the value has an unmarshaller
				var useUnmarshaller bool
				if target.Type().Implements(canUnmarshal) {
					useUnmarshaller = true
				}
				activePrefix := valueObj.activePrefix + valueObj.envSpacer
				offsetSlice := len(activePrefix)
				func() {
					// Use this to short out the walk function
					defer func() {
						// if r := recover(); r != nil {
						// 	decodeErr = r.(error)
						// }
					}()
					// Walk through the radix tree looking for prefix matches to expand into the map
					valueObj.envSpace.WalkPrefix(activePrefix, func(s string, v interface{}) bool {
						// Check on key
						fragmentedKey := v.(TreeNode).originalKey[offsetSlice:]
						// See if there is another spacer, if there is skip it as this only looks at single decedent
						if strings.Contains(fragmentedKey, valueObj.envSpacer) {
							return false
						}
						// See if this should use a custom marshal function
						if useUnmarshaller {
							tmp := reflect.New(target.Type())
							if err := tmp.Interface().(Unmarshal).UnmarshalEnv([]byte(v.(TreeNode).value), SafeTree{tree: valueObj.envSpace}); err != nil {
								// Short the walk function
								panic(err)
							}
							target.SetMapIndex(reflect.ValueOf(fragmentedKey), tmp)
						} else {
							if val, err := valueTypeOf.resolve(valueResolverMap, valueHolder{
								stringValue:  v.(TreeNode).value,
								envSpace:     valueObj.envSpace,
								jsonValue:    nil,
								activePrefix: s,
								envSpacer:    valueObj.envSpacer,
							}, target.Type().Elem()); err != nil {
								panic(err)
							} else {
								// Handle setting pointer objects rather than value objects
								// Some of the resolvers will return a pointer, some return a value
								// This ensures the correct type gets set
								if mapValueKind != reflect.Ptr && val.Type().Kind() == reflect.Ptr {
									target.SetMapIndex(reflect.ValueOf(fragmentedKey), val.Elem())
								} else {
									target.SetMapIndex(reflect.ValueOf(fragmentedKey), val)
								}
							}
						}
						return false
					})

				}()
			} else {
				err = fmt.Errorf("invalid value type of [%v] provided", mapValueKind.String())
			}
			return
		},
	},
}
