package env_test

import (
	"math"
	"os"
	"testing"

	"env"
)

type testStructString struct {
	Path string `env:"path"`
}

type testStructBool struct {
	Flag bool `env:"flag"`
}

type testStructInt struct {
	Tmpint   int   `env:"tmpint"`
	Tmpint8  int8  `env:"tmpint"`
	Tmpint16 int16 `env:"tmpint"`
	Tmpint32 int32 `env:"tmpint"`
	Tmpint64 int64 `env:"tmpint"`
}
type testStructUint struct {
	Tmpint   uint   `env:"tmpint"`
	Tmpint8  uint8  `env:"tmpint"`
	Tmpint16 uint16 `env:"tmpint"`
	Tmpint32 uint32 `env:"tmpint"`
	Tmpint64 uint64 `env:"tmpint"`
}

type testFloat struct {
	TmpFloat32 float32 `env:"tmpfloat"`
	TmpFloat64 float64 `env:"tmpfloat"`
}

type testStructArray struct {
	Arr []string `env:"tmparray"`
}

type testStructMap struct {
	M map[string]int `env:"tmpmap"`
}

type testNestedValue struct {
	TempEnv struct {
		Level1 string `env:"levelone"`
		Level  struct {
			Level2 string         `env:"two"`
			Level3 map[string]int `env:"map"`
		} `env:"level"`
	} `env:"tmpenv"`
}

type testCustomMarshal struct {
	Cm customMarshal `env:"blah"`
}

type customMarshal string

func (cm *customMarshal) UnmarshalEnv(data []byte, tree env.SafeTree) error {
	*cm = customMarshal(string(data) + "1234")
	return nil
}

const float64EqualityThreshold = 1e-9

func TestOverrideFromEnv_String(t *testing.T) {
	tmp := testStructString{}
	_ = os.Setenv("path", "/test/path")
	decoder := env.New("_", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmp.Path == "" {
		t.Errorf("Empty path found")
		t.FailNow()
	}
}

func TestOverrideFromEnv_Bool(t *testing.T) {
	tmp := testStructBool{}
	_ = os.Setenv("flag", "true")
	decoder := env.New("_", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if !tmp.Flag {
		t.Errorf("Flag has not been set.")
		t.FailNow()
	}

	// Negative test
	tmp2 := testStructBool{}
	_ = os.Setenv("flag", "blah")
	decoder.Refresh()
	if err2 := decoder.DecodeInto(&tmp2, ""); err2 == nil {
		t.Error(err2)
		t.FailNow()
	}
}

func TestOverrideFromEnv_Custom(t *testing.T) {
	tmp := testCustomMarshal{}
	_ = os.Setenv("blah", "test")
	decoder := env.New(".", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmp.Cm != "test1234" {
		t.Errorf("Invalid CM result")
		t.FailNow()
	}
}
func TestOverrideFromEnv_Int(t *testing.T) {
	tmp := testStructInt{}
	_ = os.Setenv("tmpint", "1")
	decoder := env.New(".", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmp.Tmpint != 1 {
		t.Errorf("Empty integer found")
	}
	if tmp.Tmpint != int(tmp.Tmpint8) {
		t.Errorf("Int (%v) and int8 (%v) do not match", tmp.Tmpint, tmp.Tmpint8)
	}
	if tmp.Tmpint != int(tmp.Tmpint16) {
		t.Errorf("Int (%v) and int16 (%v) do not match", tmp.Tmpint, tmp.Tmpint16)
	}
	if tmp.Tmpint != int(tmp.Tmpint32) {
		t.Errorf("Int (%v) and int32 (%v) do not match", tmp.Tmpint, tmp.Tmpint32)
	}
	if tmp.Tmpint != int(tmp.Tmpint64) {
		t.Errorf("Int (%v) and int64 (%v) do not match", tmp.Tmpint, tmp.Tmpint64)
	}
}
func TestOverrideFromEnv_Float(t *testing.T) {
	tmp := testFloat{}
	_ = os.Setenv("tmpfloat", "1.11")
	decoder := env.New(".", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if math.Abs(float64(tmp.TmpFloat32)-1.11) <= float64EqualityThreshold {
		t.Errorf("Empty integer found")
	}
	if math.Abs(float64(tmp.TmpFloat32)-tmp.TmpFloat64) <= float64EqualityThreshold {
		t.Errorf("float32 (%v) and float64 (%v) do not match", tmp.TmpFloat32, tmp.TmpFloat64)
	}
}
func TestOverrideFromEnv_Uint(t *testing.T) {
	tmp := testStructUint{}
	_ = os.Setenv("tmpint", "1")
	decoder := env.New(".", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmp.Tmpint != 1 {
		t.Errorf("Empty integer found")
	}
	if tmp.Tmpint != uint(tmp.Tmpint8) {
		t.Errorf("Uint (%v) and Uint8 (%v) do not match", tmp.Tmpint, tmp.Tmpint8)
	}
	if tmp.Tmpint != uint(tmp.Tmpint16) {
		t.Errorf("Uint (%v) and Uint16 (%v) do not match", tmp.Tmpint, tmp.Tmpint16)
	}
	if tmp.Tmpint != uint(tmp.Tmpint32) {
		t.Errorf("Uint (%v) and Uint32 (%v) do not match", tmp.Tmpint, tmp.Tmpint32)
	}
	if tmp.Tmpint != uint(tmp.Tmpint64) {
		t.Errorf("Uint (%v) and Uint64 (%v) do not match", tmp.Tmpint, tmp.Tmpint64)
	}
	_ = os.Setenv("tmpint", "-1")

	err = decoder.DecodeInto(tmp, "")
	if err == nil {
		t.Error("invalid uint provided - should have errored")
		t.FailNow()
	}
}
func TestOverrideFromEnv_Array(t *testing.T) {
	tmp := testStructArray{}
	_ = os.Setenv("tmparray", `["val1","val2"]`)
	decoder := env.New(".", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if len(tmp.Arr) != 2 {
		t.Errorf("Expected 2 items to be present in the slice only saw %v", len(tmp.Arr))
		t.FailNow()
	}
	if tmp.Arr[0] != "val1" {
		t.Errorf("Expected [val1] to be present saw [%v] instead in position [0]", tmp.Arr[0])
	}
	if tmp.Arr[1] != "val2" {
		t.Errorf("Expected [val2] to be present saw [%v] instead in position [1]", tmp.Arr[1])
	}
}

func TestOverrideFromEnv_Map(t *testing.T) {
	tmp := testStructMap{}
	_ = os.Setenv("tmpmap", `{"val1" : 1,"val2" : 2}`)
	decoder := env.New(".", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmpVal, ok := tmp.M["val1"]; !ok {
		t.Errorf("Could not find [val1] in map")
	} else {
		if tmpVal != 1 {
			t.Errorf("Expected to see [1] as the value for [val1] saw [%v] instead", tmpVal)
		}
	}
	if tmpVal, ok := tmp.M["val2"]; !ok {
		t.Errorf("Could not find [val2] in map")
	} else {
		if tmpVal != 2 {
			t.Errorf("Expected to see [2] as the value for [val2] saw [%v] instead", tmpVal)
		}
	}
}

func TestOverrideFromEnv_Nested(t *testing.T) {

	tmp := testNestedValue{}
	_ = os.Setenv("tmpenv.levelone", `test`)
	_ = os.Setenv("tmpenv.level.two", `test2`)
	_ = os.Setenv("tmpenv.level.map", `{"val1" : 1,"val2" : 2}`)
	decoder := env.New(".", "")
	err := decoder.DecodeInto(&tmp, "")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmp.TempEnv.Level1 != "test" {
		t.Errorf("Expected to see [tmp.TempEnv.Level1] set to [test] saw [%v] instead", tmp.TempEnv.Level1)
	}
	if tmp.TempEnv.Level.Level2 != "test2" {
		t.Errorf("Expected to see [tmp.TempEnv.Level.two] set to [test2] saw [%v] instead", tmp.TempEnv.Level.Level2)
	}
	if tmp.TempEnv.Level.Level3["val1"] != 1 {
		t.Errorf("Expected to see [tmp.TempEnv.Level.map] key[val1] set to [1] saw [%v] instead", tmp.TempEnv.Level.Level3["val1"])
	}
}

func TestOverrideFromEnv_NSPLusSpacer(t *testing.T) {

	tmp := testNestedValue{}
	_ = os.Setenv("random__app__tmpenv__levelone", `test`)
	_ = os.Setenv("random__app__tmpenv__level__two", `test2`)
	_ = os.Setenv("random__app__tmpenv__level__map", `{"val1" : 1,"val2" : 2}`)
	decoder := env.New("__", "random")
	err := decoder.DecodeInto(&tmp, "app")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmp.TempEnv.Level1 != "test" {
		t.Errorf("Expected to see [tmp.TempEnv.Level1] set to [test] saw [%v] instead", tmp.TempEnv.Level1)
	}
	if tmp.TempEnv.Level.Level2 != "test2" {
		t.Errorf("Expected to see [tmp.TempEnv.Level.two] set to [test2] saw [%v] instead", tmp.TempEnv.Level.Level2)
	}
	if tmp.TempEnv.Level.Level3["val1"] != 1 {
		t.Errorf("Expected to see [tmp.TempEnv.Level.map] key[val1] set to [1] saw [%v] instead", tmp.TempEnv.Level.Level3["val1"])
	}
}

func TestOverrideFromEnv_NSPLusSpacerCanonicalized(t *testing.T) {

	tmp := testNestedValue{}
	_ = os.Setenv("random__app__tmpenv__LeVeLoNe", `test`)
	_ = os.Setenv("random__app__tmpenv__level__two", `test2`)
	_ = os.Setenv("random__app__tmpenv__level__map", `{"val1" : 11,"val2" : 22}`)
	_ = os.Setenv("random__app__tmpenv", `{"levelOne" : "invalid","level" : {"two" : "invalid","map" : {"val1" : 23,"val2" : 43}}} `)
	decoder := env.New("__", "random")
	err := decoder.DecodeInto(&tmp, "app")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if tmp.TempEnv.Level1 != "test" {
		t.Errorf("Expected to see [tmp.TempEnv.Level1] set to [test] saw [%v] instead", tmp.TempEnv.Level1)
	}
	if tmp.TempEnv.Level.Level2 != "test2" {
		t.Errorf("Expected to see [tmp.TempEnv.Level.two] set to [test2] saw [%v] instead", tmp.TempEnv.Level.Level2)
	}
	if tmp.TempEnv.Level.Level3["val1"] != 11 {
		t.Errorf("Expected to see [tmp.TempEnv.Level.map] key[val1] set to [11] saw [%v] instead", tmp.TempEnv.Level.Level3["val1"])
	}
	if tmp.TempEnv.Level.Level3["val2"] != 22 {
		t.Errorf("Expected to see [tmp.TempEnv.Level.map] key[val1] set to [22] saw [%v] instead", tmp.TempEnv.Level.Level3["val1"])
	}
}

func TestOverridePathUnpacking(t *testing.T) {
	tmp := testStructMap{}
	_ = os.Setenv("random__app__tmpmap__LeVeLoNe", `2`)
	_ = os.Setenv("random__app__tmpmap__leveltwo", `32`)
	_ = os.Setenv("random__app__tmpmap__ASD2asd6aSDfa", `6123`)
	decoder := env.New("__", "random")
	err := decoder.DecodeInto(&tmp, "app")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if val, ok := tmp.M["LeVeLoNe"]; !ok || val != 2 {
		t.Errorf("expected LeVeLoNe to be set to 2, saw [%v]", val)
	}
	if val, ok := tmp.M["leveltwo"]; !ok || val != 32 {
		t.Errorf("expected leveltwo to be set to 32, saw [%v]", val)
	}
	if val, ok := tmp.M["ASD2asd6aSDfa"]; !ok || val != 6123 {
		t.Errorf("expected ASD2asd6aSDfa to be set to 6123, saw [%v]", val)
	}
}

func TestOverridePathUnpackingJSON(t *testing.T) {
	type complexMap struct {
		TmpMap map[string]struct {
			SomeElement int `env:"someElement"`
			Nested      struct {
				Item bool `env:"item"`
			} `env:"nested"`
		} `env:"tmpMap"`
	}
	tmp := complexMap{}
	_ = os.Setenv("random__app__tmpmap__LeVeLoNe", `{"someElement" : 1, "nested" : {"item" : true}}`)
	_ = os.Setenv("random__app__tmpmap__leveltwo", `{"someElement" : 11, "nested" : {"item" : false}}`)
	_ = os.Setenv("random__app__tmpmap__ASD2asd6aSDfa", `{"someElement" : 122, "nested" : {"item" : true}}`)
	decoder := env.New("__", "random")
	err := decoder.DecodeInto(&tmp, "app")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	if val, ok := tmp.TmpMap["LeVeLoNe"]; !ok || val.SomeElement != 1 || val.Nested.Item != true {
		t.Errorf("expected LeVeLoNe to be set to [%v], saw [%v]", `{"someElement" : 1, "nested" : {"item" : true}}}`, val)
	}
	if val, ok := tmp.TmpMap["leveltwo"]; !ok || val.SomeElement != 11 || val.Nested.Item != false {
		t.Errorf("expected leveltwo to be set to [%v], saw [%v]", `{"someElement" : 11, "nested" : {"item" : false}}}`, val)
	}
	if val, ok := tmp.TmpMap["ASD2asd6aSDfa"]; !ok || val.SomeElement != 122 || val.Nested.Item != true {
		t.Errorf("expected ASD2asd6aSDfa to be set to [%v], saw [%v]", `{"someElement" : 122, "nested" : {"item" : true}}}`, val)
	}
}
