module bitbucket.org/weisbartb/env

go 1.12

require (
	github.com/armon/go-radix v1.0.0
	github.com/valyala/fastjson v1.4.1
)
