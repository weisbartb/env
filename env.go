package env

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strings"
	"sync/atomic"
	"unsafe"

	"github.com/armon/go-radix"
	"github.com/valyala/fastjson"
)

const (
	maxInt8   = 0xFF >> 1
	maxInt16  = 0xFFFF >> 1
	maxInt32  = 0xFFFFFFFF >> 1
	maxInt64  = 0xFFFFFFFFFFFFFFFF >> 1
	maxUint8  = 0xFF
	maxUint16 = 0xFFFF
	maxUint32 = 0xFFFFFFFF
	maxUint64 = 0xFFFFFFFFFFFFFFFF
)

var ErrCanNotResolveNonSettable = errors.New("please provide a pointer not a value")
var ErrTooMuchIndirection = errors.New("reflected value has too much indirection")

var canUnmarshal = reflect.TypeOf(new(Unmarshal)).Elem()

type KeyResolver func(key []byte, target reflect.Value) error
type swappableStringSlice struct {
	items []string
}
type Wrapper struct {
	fallbacks  []string
	separator  string
	namespace  string
	osEnvVars  *swappableStringSlice
	searchTree *radix.Tree
}
type TreeNode struct {
	originalKey string
	value       string
}

func New(separator, namespace string) Wrapper {
	wrapper := Wrapper{
		separator:  separator,
		namespace:  namespace,
		searchTree: radix.New(),
		osEnvVars:  &swappableStringSlice{items: nil},
	}
	wrapper.Refresh()
	return wrapper
}
func (w *Wrapper) Refresh() {
	searchTree := radix.New()
	tmpVars := &swappableStringSlice{
		items: os.Environ(),
	}
	for _, v := range tmpVars.items {
		parts := strings.SplitN(v, "=", 2)
		if len(parts) != 2 {
			// Broken
			continue
		}
		key := parts[0]
		value := parts[1]
		searchTree.Insert(strings.ToLower(key), TreeNode{originalKey: key, value: value})
	}
	atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(&w.searchTree)), unsafe.Pointer(searchTree))
	atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(&w.osEnvVars)), unsafe.Pointer(tmpVars))
}
func (w *Wrapper) SetFallbacks(tags []string) {
	w.fallbacks = tags
}
func (w Wrapper) DecodeInto(i interface{}, basePath string) error {
	if len(w.namespace) > 0 {
		return w.decode(i, w.namespace+w.separator+basePath+w.separator)
	} else {
		return w.decode(i, basePath)
	}
}
func (w Wrapper) decode(i interface{}, basePath string) error {
	var v reflect.Value
	var ok bool
	if v, ok = i.(reflect.Value); !ok {
		v = reflect.ValueOf(i)
	}
	var ct = 5
ptrResolver:
	// Resolve any pointers to their actual underlying types
	if v.Type().Kind() == reflect.Ptr {
		v = v.Elem()
		ct--
		if ct == 0 && v.Type().Kind() == reflect.Ptr {
			return ErrTooMuchIndirection
		}
		goto ptrResolver
	}
	// Look to see if the current interface target is a struct
	// If so look for all of its children that can be set
	if v.Type().Kind() == reflect.Struct && v.Type().NumField() > 0 {
		numFields := v.NumField()
		if !v.CanSet() {
			return ErrCanNotResolveNonSettable
		}
		for x := 0; x < numFields; x++ {
			// See if it can even be set
			if !v.Field(x).CanSet() {
				continue
			}
			// See what type of tags it has
			tags := parseTags(string(v.Type().Field(x).Tag))
			var tagVal string
			var tagFound bool
			// See if the "env" tag is present
			if tagVal, tagFound = tags["env"]; !tagFound {
				// Look through other configured fallbacks
				for _, fallbackTag := range w.fallbacks {
					if tagVal, tagFound = tags[fallbackTag]; tagFound {
						break
					}
				}
				if !tagFound {
					// No tags found - ignore the field
					continue
				}
			}
			var fieldTarget string
			// See if there is a base path to prepend onto the child element
			if len(basePath) > 0 {
				fieldTarget = strings.ToLower(basePath + tagVal)
			} else {
				fieldTarget = strings.ToLower(tagVal)
			}
			if node, found := w.searchTree.Get(fieldTarget); found {
				treeNode := node.(TreeNode)
				// See if a custom unmarshaller is supported for this field
				if reflect.PtrTo(v.Field(x).Type()).Implements(canUnmarshal) {
					t := v.Field(x).Interface()
					_ = t
					var val reflect.Value
					var set bool
					if v.Field(x).Kind() == reflect.Ptr {
						val = v.Field(x)
					} else {
						val = reflect.New(v.Field(x).Type())
						set = true
					}
					if err := val.Interface().(Unmarshal).UnmarshalEnv([]byte(treeNode.value), SafeTree{tree: w.searchTree}); err != nil {
						return err
					}
					if set {
						if !v.Field(x).CanSet() {
							return ErrCanNotResolveNonSettable
						} else {
							v.Field(x).Set(val.Elem())
						}
					}
				} else {
					// Handle complex types
					if v.Field(x).Kind() == reflect.Struct {
						// See if this is a JSON object
						if len(treeNode.value) > 0 && (treeNode.value[0] == '{' || treeNode.value[0] == '[') {
							// Create a new value holder for this pointer
							// It is possible to do this without a temporary holder, however if an error occurs it could
							//	break the initial structure.
							newObj := reflect.New(v.Field(x).Type()).Interface()
							// Try to decode it - if there is an error throw the error
							if decodeErr := json.Unmarshal([]byte(treeNode.value), newObj); decodeErr != nil {
								return fmt.Errorf("error while decoding key %v - %v", fieldTarget, decodeErr)
							}
							v.Field(x).Set(reflect.ValueOf(newObj).Elem())
						}
						// Apply any additional overrides that supersede the json map
						err := w.decode(v.Field(x), fieldTarget+w.separator)
						if err != nil {
							return err
						}
						continue
					}
					// Handle primitives that do not have custom unmarshalling
					if resolver, allowedValueFound := allowedValues[v.Field(x).Kind()]; allowedValueFound {
						// Create a value holder with the string loaded
						holder := valueHolder{stringValue: treeNode.value, envSpace: w.searchTree, activePrefix: fieldTarget, envSpacer: w.separator}
						// See if the value is a JSON data type
						if treeNode.value[0] == '{' || treeNode.value[0] == '[' {
							// Create a fast JSON parser to hold the data
							fastParser := fastjson.Parser{}
							// Parse the JSON values
							if jsonData, err := fastParser.Parse(treeNode.value); err != nil {
								return err
							} else {
								holder.jsonValue = jsonData
							}
						}
						if setVal, err := resolver.resolve(allowedValues, holder, v.Field(x).Type()); err != nil {
							return err
						} else {
							v.Field(x).Set(setVal)
						}
					} else {
						return ErrUnableToResolve
					}
				}
			} else {
				// See if its a struct and may have additional items that need to be parsed through
				switch v.Field(x).Kind() {
				case reflect.Struct:
					err := w.decode(v.Field(x), fieldTarget+w.separator)
					if err != nil {
						return err
					}
				case reflect.Map:
					if val, err := allowedValues[reflect.Map].resolve(allowedValues, valueHolder{
						stringValue:  "",
						envSpace:     w.searchTree,
						activePrefix: fieldTarget,
						envSpacer:    w.separator,
					}, v.Field(x).Type()); err != nil {
						return err
					} else {
						if !val.IsNil() {
							v.Field(x).Set(val)
						}
					}
				}
			}
		}
	}

	return nil
}
